const envia = () => {
  const txt = document.querySelector('.text-box').value
  const body = document.querySelector('.chat')
  const msg = document.createElement("div")
  msg.classList.add("msg")
  msg.innerHTML = `<p>${txt} </p>
          <div class="options">
            <button class="button edt" onclick="edita(this)">Editar</button>
            <button class="button exc" onclick="exclui(this)">Excluir</button>
          </div>
`
  body.appendChild(msg)
  document.querySelector('.text-box').value = ""
}

const exclui = (elemento) => {
  elemento.parentElement.parentElement.remove()
}

const edita = (elemento) => {
  const msg = elemento.parentElement.parentElement.querySelector('p')
  const input = document.createElement('input')
  input.value = msg.innerHTML
  msg.parentNode.replaceChild(input, msg)
  elemento.setAttribute("onclick", "salva(this)")
  elemento.innerHTML = "salvar"
  elemento.style.background = "green"
}

const salva = (elemento) => {
  const input = elemento.parentElement.parentElement.querySelector('input')
  const msg = document.createElement('p')
  msg.innerHTML = input.value
  input.parentNode.replaceChild(msg, input)
  elemento.setAttribute("onclick", "edita(this)")
  elemento.innerHTML = "Edita"
  elemento.style.background = "yellow"
  console.log("salvo")
}
/*

*/
